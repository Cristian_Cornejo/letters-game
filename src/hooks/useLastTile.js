import { useState } from 'react'

const useLastTile = () => {

    const [lastTile, setLastTile] = useState(null);
    const isNeighborOfTheLastTile = (tile) => {
        return (Math.abs(lastTile.x - tile.x) <= 1 && Math.abs(lastTile.y - tile.y) <= 1)
    }

    return [setLastTile, isNeighborOfTheLastTile]
}

export default useLastTile;