import { ERROR_BOARD, GET_BOARD, LOADING_BOARD } from "../types/boardTypes";
import { useDispatch } from 'react-redux';
import { useEffect } from "react";
const useBoard = () => {
    const dispatch = useDispatch();
    useEffect(() => {

        const fetchData = async () => {
            const response = await fetch('test-board-1.json');
            const { board } = await response.json();
            return board.map(letter => letter.toLowerCase());;
        }

        dispatch({
            type: LOADING_BOARD
        })

        try {
            fetchData().then((board) => dispatch({
                type: GET_BOARD,
                payload: board
            }));

        } catch (error) {
            dispatch({
                type: ERROR_BOARD,
                payload: error.message

            })
        }
    }, [dispatch])

}

export default useBoard;