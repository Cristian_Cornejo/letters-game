import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { getRandomFromArray, shuffle } from '../utils/arrayUtils';
const useArrayWithCoordinate = () => {
    const data = useSelector((state) => state.boardReducer.board);
    const dictionary = useSelector((state) => state.dictionaryReducer.dictionary);
    const randomWord2 = useSelector((state) => state.dictionaryReducer.selectedWord);
    const toggleTileColor = (tile) => {
        setTilesArray(ts => ts.map(t => JSON.stringify(t) === JSON.stringify(tile) ? { ...t, isSelected: !t.isSelected } : t))
    }

    const [tilesArray, setTilesArray] = useState([]);

    const resetData = () => {
        setTilesArray(addBoardData(data, randomWord2));
    }

    const isSomeTileSelected = () => {
        return tilesArray.find(t => t.isSelected);
    }

    useEffect(() => {
        setTilesArray(addBoardData(data, randomWord2));
    }, [data, dictionary, randomWord2])

    return [tilesArray, toggleTileColor, resetData, isSomeTileSelected];
}

const addBoardData = (data, randomWord2) => {
    const newArrayWithCoordinate = addCoordinateToArray(data);
    const newArrayWithRandomWord = addRandomWordToArray(randomWord2, newArrayWithCoordinate);
    return newArrayWithRandomWord;
}

const addCoordinateToArray = (array) => {
    const shuffledArray = shuffle(array);
    const newArrayWithCoordinate = [];
    for (let y = 0; y < 4; y++) {
        for (let x = 0; x < 4; x++) {
            const value = shuffledArray[4 * y + x];
            newArrayWithCoordinate.push({ value, x, y, isSelected: false })
        }
    }

    return newArrayWithCoordinate;
}

const addRandomWordToArray = (randomWord, arrayWithCoordinate) => {
    let array = [...arrayWithCoordinate];
    let usedTilesForWord = [];
    if (randomWord) {
        let indexOfRandomTile = null;
        let randomTile = null;
        for (let index = 0; index < randomWord.length; index++) {
            const letter = randomWord[index];
            if (index === 0) {
                randomTile = getRandomFromArray(array);
                indexOfRandomTile = array.indexOf(randomTile);
                array[indexOfRandomTile].value = letter;
            } else {
                usedTilesForWord = [...usedTilesForWord, randomTile];
                const calculatedRandomNeighbors = calculateRandomNeighbors(randomTile, usedTilesForWord);
                if (calculatedRandomNeighbors.length < 1) {
                    usedTilesForWord = [];
                    indexOfRandomTile = null;
                    randomTile = null;
                    index = -1;
                    array = arrayWithCoordinate;
                } else {
                    const randomPosition = getRandomFromArray(calculatedRandomNeighbors);
                    randomTile = array.find(tile => tile.x === randomPosition.x && tile.y === randomPosition.y);
                    indexOfRandomTile = array.indexOf(randomTile);
                    array[indexOfRandomTile].value = letter;
                }
            }
        }
    }
    return array;
}

const calculateRandomNeighbors = (randomTile, usedTilesForWord) => {
    const validNeighbors = defaultNeighborDistance.reduce(
        (acc, { x, y }) => {
            const newX = randomTile.x + x;
            const newY = randomTile.y + y;
            if (newX > 0 && newX < 4 && newY > 0 && newY < 4) {
                acc = [...acc, { x: newX, y: newY }]
            }
            return acc;
        },
        []
    );
    const filteredValidNeighbors = validNeighbors
        .filter(validNeighbor => !usedTilesForWord
            .find(usedTileForWord => usedTileForWord.x === validNeighbor.x && usedTileForWord.y === validNeighbor.y));
    return filteredValidNeighbors;
}

const defaultNeighborDistance = [
    { x: -1, y: -1 },
    { x: 0, y: -1 },
    { x: 1, y: -1 },
    { x: -1, y: 0 },
    { x: 1, y: 0 },
    { x: -1, y: 1 },
    { x: 0, y: 1 },
    { x: 1, y: 1 },
]
export default useArrayWithCoordinate;

