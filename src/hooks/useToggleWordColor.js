import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
const useToggleWordColor = () => {
    const randomWord = useSelector((state) => state.dictionaryReducer.selectedWord);
    const [word, setWord] = useState('');
    const [validWord, setValidWord] = useState(false);

    const resetWord = () => {
        setWord('');
    }

    useEffect(() => {
        setValidWord(existsInDictionary(word, randomWord));
    }, [word, randomWord])

    return { word, setWord, resetWord, validWord };
}

const existsInDictionary = (word, randomWord) => randomWord === word;

export default useToggleWordColor;