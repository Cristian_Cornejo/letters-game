import { ERROR_DICTIONARY, GET_DICTIONARY, LOADING_DICTIONARY, RANDOM_DICTIONARY_WORD } from "../types/dictionaryTypes";
import { useDispatch } from 'react-redux';
import { useEffect } from "react";
const useDictionary = () => {
    const dispatch = useDispatch();
    const getRandomWord = () => {
        dispatch({
            type: RANDOM_DICTIONARY_WORD
        })
    }
    useEffect(() => {

        const fetchData = async () => {
            const response = await fetch('dictionary.json');
            const { words } = await response.json();
            return words.map(word => word.toLowerCase());
        }

        dispatch({
            type: LOADING_DICTIONARY
        })

        try {
            fetchData().then((words) => dispatch({
                type: GET_DICTIONARY,
                payload: words
            }));

        } catch (error) {
            dispatch({
                type: ERROR_DICTIONARY,
                payload: error.message

            })
        }
    }, [dispatch])
    return [getRandomWord]
}

export default useDictionary;