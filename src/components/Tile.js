import React from 'react';
import styled from 'styled-components';

const Tile = ({ tile, onClickTile }) => {
    return (<StyledTile selected={tile.isSelected} onClick={() => onClickTile(tile)}>
        {tile.value}
    </StyledTile>);
}

const StyledTile = styled.div`
    background-color: black;
    color: white;
    width: 72px;
    height: 72px;
    display: flex;
    justify-content: center;
    align-items: center;
    border: 2px solid red;
    border-radius: 10%;

    font-size: 48px;
    font-weight: bold;
    box-sizing: border-box;
    text-transform: uppercase;
    background: ${props => props.selected ?
        'linear-gradient(0deg, rgba(221, 87, 25, 1) 0%, rgb(211, 54, 15) 100%)' :
        'linear-gradient(0deg, rgba(253, 157, 45, 1) 0%, rgba(253, 187, 45, 1) 100%)'};
`

export default Tile;
