import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';


const HeaderBoard = ({ restart }) => {
    const randomWord2 = useSelector((state) => state.dictionaryReducer.selectedWord);
    return (<StyledHeaderBoard>
        <div>{randomWord2}</div>
        <button onClick={restart}>clear word <i>x</i></button>
    </StyledHeaderBoard>);
}

const StyledHeaderBoard = styled.div`
    box-sizing: border-box;
    display: flex;
    align-items: center;
    width: 100%;
    height: 100%;
    grid-column: 1 / 5;
    margin-bottom: 2rem;
    button {
        margin-left: auto;
        border: none;
        background-color: transparent;
        color: white;
    }

    & > div {
        font-size: 32px;
        font-weight: bold;
        color: white;
    }

    i {
        background-color: grey;
        font-style: normal;
        box-sizing: border-box;
        padding: 4px 8px;
        border-radius: 50%;
    }
`
export default HeaderBoard;