import React from 'react';
import HeaderBoard from './HeaderBoard';
import ResultWord from './ResultWord';
import styled from 'styled-components';

const Board = ({ children, validWord, word, restart }) => {
    return (
        <StyledBoard>
            <HeaderBoard restart={restart}></HeaderBoard>
            {children}
            <ResultWord word={word} validWord={validWord}></ResultWord>
        </StyledBoard>
    )
}

const StyledBoard = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    gap: 8px;
`
export default Board;