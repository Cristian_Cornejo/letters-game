import React from 'react';
import styled from 'styled-components';

const ResultWord = ({ validWord, word }) => {
    return (<StyledResultWord>
        <StyledWord validWord={validWord}>{word}</StyledWord>
        <StyledValidWord validWord={validWord}>{validWord ? 'valid' : 'invalid'}</StyledValidWord>
    </StyledResultWord>);
}

const StyledResultWord = styled.div`
    margin-top: 4em;
    grid-column: 1 / 5;
    box-sizing: border-box;
    display: flex;
    border: 1px solid white;
    width: 100%;
    height: 72px;
    align-items: center;
    padding-inline-start: 2.5em;
    padding-inline-end: 1.5em;
    `

const StyledValidWord = styled.p`
    margin-left: auto;
    color: ${props => props.validWord ? 'green' : 'white'};
`

const StyledWord = styled.p`
    font-size: 48px;
    font-weight: bold;
    box-sizing: border-box;
    margin-block-start: 0;
    margin-block-end: 0;
    text-transform: uppercase;
    color: ${props => props.validWord ? 'green' : 'white'};
`

export default ResultWord;

