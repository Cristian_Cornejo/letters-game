import { ERROR_BOARD, GET_BOARD, LOADING_BOARD } from "../types/boardTypes";

const initialState = {
    board: [],
    loading: false,
    error: null
}

const boardReducer = (state = initialState, { type, payload }) => {
    switch (type) {

        case GET_BOARD:
            return {
                ...state,
                loading: false,
                error: null,
                board: payload
            }
        case ERROR_BOARD:
            return {
                ...state,
                loading: false,
                error: payload
            }
        case LOADING_BOARD:
            return {
                ...state,
                loading: true
            }
        default:
            return state
    }
}
export default boardReducer;
