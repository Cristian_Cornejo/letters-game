import { ERROR_DICTIONARY, GET_DICTIONARY, LOADING_DICTIONARY, RANDOM_DICTIONARY_WORD } from "../types/dictionaryTypes";
import { getRandomFromArray } from "../utils/arrayUtils";
const initialState = {
    dictionary: [],
    loading: false,
    error: null,
    selectedWord: '',
}

const dictionaryReducer = (state = initialState, { type, payload }) => {
    switch (type) {

        case GET_DICTIONARY:
            return {
                ...state,
                loading: false,
                error: null,
                dictionary: payload,
                selectedWord: getRandomFromArray(payload)
            }
        case ERROR_DICTIONARY:
            return {
                ...state,
                loading: false,
                error: payload
            }
        case LOADING_DICTIONARY:
            return {
                ...state,
                loading: true
            }
        case RANDOM_DICTIONARY_WORD:
            return {
                ...state,
                selectedWord: getRandomFromArray(state.dictionary)
            }
        default:
            return state
    }
}
export default dictionaryReducer;
