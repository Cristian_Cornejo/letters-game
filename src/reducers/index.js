import { combineReducers } from "redux";
import dictionaryReducer from "./dictionaryReducer";
import boardReducer from './boardReducer';
export default combineReducers({
    dictionaryReducer,
    boardReducer
});