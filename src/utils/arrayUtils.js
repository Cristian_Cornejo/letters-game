export const getRandomFromArray = (array) => {
    return array[Math.floor(Math.random() * array.length)];
}


export const shuffle = (array) => {
    const shuffledArray = [];

    for (let index = 0; index < array.length; index++) {
        const randomTile = getRandomFromArray(array);
        shuffledArray.push(randomTile);
    }

    return shuffledArray;
}