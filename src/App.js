import { useCallback } from 'react';
import Board from './components/Board';
import Tile from './components/Tile';
import useArrayWithCoordinate from './hooks/useArrayWithCoordinate';
import useLastTile from './hooks/useLastTile';
import useToggleWordColor from './hooks/useToggleWordColor';
import useDictionary from './hooks/useDictionary';
import useBoard from './hooks/useBoard';

import { createGlobalStyle } from 'styled-components';
const App = () => {
  const [getRandomWord] = useDictionary();
  useBoard();
  const [setLastTile, isNeighborOfTheLastTile] = useLastTile();
  const { word, setWord, resetWord, validWord } = useToggleWordColor();
  const [tilesArray, toggleTileColor, resetData, isSomeTileSelected] = useArrayWithCoordinate();

  const onClickTile = useCallback(
    (tile) => {
      if (!tile.isSelected && !validWord) {
        if (!isSomeTileSelected() || (isNeighborOfTheLastTile(tile) && isSomeTileSelected())) {
          toggleTileColor(tile);
          setLastTile(tile);
          setWord(word => `${word}${tile.value}`)
        }
      }
    }, [toggleTileColor, setWord, setLastTile, isSomeTileSelected, isNeighborOfTheLastTile, validWord])

  const restart = () => {
    resetWord();
    resetData();
    getRandomWord();
  }

  return (
    <>
      <GlobalStyle />
      <div className="App">
        <Board word={word} validWord={validWord} restart={restart}>
          {tilesArray.map((tile, i) =>
            <Tile key={i} onClickTile={onClickTile} tile={tile}></Tile>)}
        </Board>
      </div>
    </>
  );
}

const GlobalStyle = createGlobalStyle`
body {
  margin: 0;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  height: 100%;
}

html,
#root {
  height: 100%;
}

code {
  font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
}

.App {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  background-color: black;
}
`;

export default App;
